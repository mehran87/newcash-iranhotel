<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return array[]|\Illuminate\Http\Response
     */
    public function get_cities()
    {
        /*
        * 1- get states with cities
        * 2- set Authorization and send validated data to Http facade and get data
        * 3- check success response
        * 4- return response
        *
        *  */

//        $response = Http::withHeaders([
//            'Authorization' => 'Bearer ' . env('IRAN_HOTEL_JWT_TOKEN'),
//            'Accept'        => 'application/json',
//        ])->get('https://apiout.iranhotelonline.com/api/app/v1/utilities/states');
//
//        if ($response->successful()) {
//            return response()->json(['data' => $response], 200);
//        }
//        else{
//            return  $response->body();
//        }

        return [
            [
                "Id"     => 0,
                "Title"  => "string",
                "Cities" => [
                    [
                        "Id"    => 0,
                        "Title" => "string"
                    ]
                ],
            ],
        ];
    }

    public function get_nationalities()
    {
        /*
* 1- get states with cities
* 2- set Authorization and send validated data to Http facade and get data
* 3- check success response
* 4- return response
*
*  */

//        $response = Http::withHeaders([
//            'Authorization' => 'Bearer ' . env('IRAN_HOTEL_JWT_TOKEN'),
//            'Accept'        => 'application/json',
//        ])->get('https://apiout.iranhotelonline.com/api/app/v1/utilities/nationalities');
//
//        if ($response->successful()) {
//            return response()->json(['data' => $response], 200);
//        }
//        else{
//            return  $response->body();
//        }

        return [
            [
                "Id"    => 0,
                "Title" => "string"
            ]
        ];
    }

}
