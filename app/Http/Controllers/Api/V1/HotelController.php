<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchHotelRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HotelController extends Controller
{

    public function external_search(SearchHotelRequest $request)
    {

        /*
         * 1- get data from request class
         * 2- set Authorization and send validated data to Http facade and get data
         * 3- check success response
         *
         *  */
//        $response = Http::withHeaders([
//            'Authorization' => 'Bearer ' . env('IRAN_HOTEL_JWT_TOKEN'),
//            'Accept'        => 'application/json',
//        ])->post('http://localhost:50418/api/app/v1/hotel/externalSearch', $request->validated());
//
//
//        if ($response->successful()) {
//            return response()->json(['data' => $response], 200);
//        }
//        else{
//            return  $response->body();
//        }

        return
            [
                "pageIndex"  => 0,
                "pageSize"   => 10,
                "totalItems" => 1,
                "data"       => [
                    [
                        "id"              => 1184,
                        "name"            => "ائل آی",
                        "hotelRank"       => "3.9",
                        "address"         => "تبریز – شهرک باغمیشه – خیابان الهیه – انتهای خیابان سهند جنوبی",
                        "rankName"        => "بسیار خوب",
                        "gradeName"       => "0",
                        "boardPrice"      => 8800000,
                        "ihoPrice"        => 8420000,
                        "hasFreeTransfer" => false,
                        "fullName"        => "هتل آپارتمان ائل آی تبریز"
                    ]
                ]
            ];


    }

    public function get_hotel_information(Request $request, $hotel_id)
    {
        /*
         * 1- get hotel id from url
         * 2- set Authorization and send validated data with hotel ID to Http facade and get data
         * 3- check success response
         * 4- return response
         *
         *  */

//        $response = Http::withHeaders([
//            'Authorization' => 'Bearer ' . env('IRAN_HOTEL_JWT_TOKEN'),
//            'Accept'        => 'application/json',
//        ])->get('http://localhost:50418/api/app/v1/hotel/'.$hotel_id, $request->input());
//
//
//        if ($response->successful()) {
//            return response()->json(['data' => $response], 200);
//        }
//        else{
//            return  $response->body();
//        }

        return [
            "Name"                       => "string",
            "Wifi"                       => true,
            "WifiDescription"            => "string",
            "Parking"                    => true,
            "ParkingDescription"         => "string",
            "Gallery"                    => [
                ["Title" => "string", "TitleEn" => "string", "PictureUrl" => "string"],
            ],
            "Checkin"                    => "string",
            "Checkout"                   => "string",
            "SeasonNotes"                => ["string"],
            "CancellationPolicy"         => "string",
            "ChildrenCancellationPolicy" => "string",
            "OtherNotes"                 => "string",
            "NoroozNotes"                => ["string"],
            "About"                      => "string",
            "Location"                   => ["Lat" => "string", "Lng" => "string"],
            "Restaurant"                 => "string",
            "FacilityCategories"         => [
                ["Name" => "string", "facilities" => [["Name" => "string"]]],
            ],
            "Rates"                      => [["Question" => "string", "Rate" => "string"]],
            "FreeWifi"                   => true,
            "FreeParking"                => true,
            "Id"                         => 0,
            "HotelRank"                  => "string",
            "CityName"                   => "string",
            "StateName"                  => "string",
            "Address"                    => "string",
            "TypeName"                   => "string",
            "RankName"                   => "string",
            "GradeName"                  => "string",
            "BoardPrice"                 => 0,
            "IhoPrice"                   => 0,
            "HasFreeTransfer"            => true,
            "FullName"                   => "string",
        ];
    }

    public function get_rooms(Request $request, $hotel_id)
    {
        /*
         * 1- get hotel id from url
         * 2- set Authorization and send validated data with hotel ID to Http facade and get data
         * 3- check success response
         * 4- return response
         *
         *  */

//        $response = Http::withHeaders([
//            'Authorization' => 'Bearer ' . env('IRAN_HOTEL_JWT_TOKEN'),
//            'Accept'        => 'application/json',
//        ])->get('http://localhost:50418/api/app/v1/hotel/'.$hotel_id.'/getRooms', $request->input());
//
//
//        if ($response->successful()) {
//            return response()->json(['data' => $response], 200);
//        }
//        else{
//            return  $response->body();
//        }

        return [
            [
                "Availability"              => [
                    [
                        "CurrentDate"  => "2019-08-24T14:15:22Z",
                        "Full"         => true,
                        "Blocked"      => true,
                        "HasNoEnter"   => true,
                        "HasNoExit"    => true,
                        "Closed"       => true,
                        "IsPackage"    => true,
                        "FreeCapacity" => 0,
                        "RoomId"       => 0,
                    ],
                ],
                "Gallery"                   => [["Title" => "string", "PictureUrl" => "string"]],
                "Id"                        => 0,
                "Name"                      => "string",
                "ExtraCapacity"             => 0,
                "AdultCapacity"             => 0,
                "AdultCapacityDescription"  => "string",
                "InfantCapacity"            => 0,
                "InfantCapacityDescription" => "string",
                "Description"               => "string",
                "RoomSize"                  => "string",
                "BedSize"                   => "string",
                "Facilities"                => [["Title" => "string", "Description" => "string"]],
                "Prices"                    => [
                    [
                        "Nights"        => [
                            [
                                "CurrentDate"     => "string",
                                "BoardPrice"      => 0,
                                "IhoPrice"        => 0,
                                "MinSalePrice"    => 0,
                                "CommissionPrice" => 0,
                                "ExtraBoardPrice" => 0,
                                "ExtraIhoPrice"   => 0,
                                "BreakFast"       => true,
                                "Lunch"           => true,
                                "Dinner"          => true,
                            ],
                        ],
                        "Promotions"    => [
                            [
                                "Model"         => "string",
                                "NonRefundable" => true,
                                "ExpireDate"    => "2019-08-24T14:15:22Z",
                            ],
                        ],
                        "NonRefundable" => true,
                    ],
                ],
                "HasEnter"                  => true,
                "HasExit"                   => true,
            ],
        ];

    }

}
