<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchHotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stateId'   => 'nullable|numeric|exists:states,id',
            'cityId'    => 'nullable|numeric|exists:states,city_id',
            'hotelId'   => '',
            'startDate' => '1402/11/27',
            'endDate'   => '1402/11/30',
            'pageIndex' => ' 0',
            'pageSize'  => ' 10',
        ];
    }
}
