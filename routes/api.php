<?php

use App\Http\Controllers\Api\V1\HotelController;
use App\Http\Controllers\Api\V1\StateController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'hotel'], function () {
    Route::post('search', [HotelController::class, 'external_search']);
    Route::get('/{id}', [HotelController::class, 'get_hotel_information']);
    Route::get('{id}/getRooms',[HotelController::class,'']);
});

Route::group(['prefix' => 'utilities'], function (){
    Route::get('states', [StateController::class, 'get_cities']);
    Route::get('nationalities', [StateController::class, 'get_nationalities']);
});

